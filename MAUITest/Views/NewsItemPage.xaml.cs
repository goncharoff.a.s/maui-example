﻿using MAUITest.ViewModels.NewsItem;

namespace MAUITest.Views;

public partial class NewsItemPage : ContentPage
{
	public NewsItemPage(NewsItemPageVM pageVM)
	{
		InitializeComponent();
		this.BindingContext = pageVM;
    }
}
