﻿using MAUITest.ViewModels;
using MAUITest.ViewModels.NewsItem;
using MAUITest.ViewModels.NewsPage;

namespace MAUITest.Views;

public partial class NewsPage : ContentPage, INewsPage
{
	public NewsPage()
	{
		InitializeComponent();
        this.BindingContext = new NewsPageVM(this);
	}

    public void ShowItemNewsPage(NewsItemPageVM pageVM)
    {
        this.Navigation.PushAsync(new NewsItemPage(pageVM));
    }

    public void ShowLoginPage()
    {
        Application.Current.MainPage = new NavigationPage(new LoginPage());
    }
}


