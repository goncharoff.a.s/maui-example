﻿using MAUITest.ViewModels;
using MAUITest.ViewModels.Login;

namespace MAUITest.Views;

public partial class LoginPage : ContentPage, ILoginPage
{
	public LoginPage()
	{
		InitializeComponent();

		this.BindingContext = new LoginPageVM(this);
    }

    public void OpenMainPage()
    {
        Application.Current.MainPage = new NavigationPage(new NewsPage());
    }
}
