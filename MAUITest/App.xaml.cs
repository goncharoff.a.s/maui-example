﻿using MAUITest.Services;
using MAUITest.ViewModels.NewsPage;
using MAUITest.Views;

namespace MAUITest;

public partial class App : Application
{
	public App()
	{
		InitializeComponent();

        if (Service<ILoginService>.GetInstance().IsLogin)
        {
            MainPage = new NavigationPage(new NewsPage());
        }
        else
        {
            MainPage = new NavigationPage(new LoginPage());
        }
    }
}

