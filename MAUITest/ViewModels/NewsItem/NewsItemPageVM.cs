﻿using System;
using System.ComponentModel;
using MAUITest.Services.DTOs;

namespace MAUITest.ViewModels.NewsItem
{
    public class NewsItemPageVM : INotifyPropertyChanged
    {
        private string _title;
        public string Title
        {
            get
            {
                return _title;
            }
            set
            {
                if (_title != value)
                {
                    _title = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Title)));
                }
            }
        }

        private string _content;
        public string Content
        {
            get
            {
                return _content;
            }
            set
            {
                if (_content != value)
                {
                    _content = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Content)));
                }
            }
        }

        private string _url;
        public string URL
        {
            get
            {
                return _url;
            }
            set
            {
                if (_url != value)
                {
                    _url = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(URL)));
                }
            }
        }

        private bool _isLoading;
        public bool IsLoading
        {
            get
            {
                return _isLoading;
            }
            set
            {
                if (_isLoading != value)
                {
                    _isLoading = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(IsLoading)));
                }
            }
        }

        private string _imageUrl;
        public string ImageUrl
        {
            get
            {
                return _imageUrl;
            }
            set
            {
                if (_imageUrl != value)
                {
                    _imageUrl = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(ImageUrl)));
                }
            }
        }

        private ListItemNewsDTO _newsDTO;

        public event PropertyChangedEventHandler PropertyChanged;

        public NewsItemPageVM(ListItemNewsDTO newsDTO)
        {
            _newsDTO = newsDTO;

            LoadNews();
        }

        private async void LoadNews()
        {
            try
            {
                this.IsLoading = true;
                var content = await Context.NewsService.LoadNews(_newsDTO.Id);

                URL = content.Url;
                Title = content.Title;
                Content = content.Content;
                ImageUrl = content.UrlToImage;
            }
            catch (Exception ex)
            {
                await Context.NotificationService.ShowNotification(ex.ToString());
            }
            finally
            {
                this.IsLoading = false;
            }
        }
    }
}