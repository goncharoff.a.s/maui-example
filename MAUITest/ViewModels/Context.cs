﻿using System;
using MAUITest.Services;

namespace MAUITest.ViewModels
{

    public static class Context
    {
       public static INewsService NewsService => Service<INewsService>.GetInstance();
       public static ILoginService LoginService => Service<ILoginService>.GetInstance();
       public static INotificationService NotificationService => Service<INotificationService>.GetInstance();
    }
}

