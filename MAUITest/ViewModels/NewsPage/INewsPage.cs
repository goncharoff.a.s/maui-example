﻿using System;
using MAUITest.ViewModels.NewsItem;

namespace MAUITest.ViewModels.NewsPage
{
	public interface INewsPage
	{
		public void ShowLoginPage();
		public void ShowItemNewsPage(NewsItemPageVM pageVM);
	}
}

