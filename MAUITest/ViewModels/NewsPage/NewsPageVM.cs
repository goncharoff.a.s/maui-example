﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows.Input;
using MAUITest.ViewModels.NewsItem;
using MAUITest.ViewModels.NewsPage;

namespace MAUITest.ViewModels
{
	public class NewsPageVM : INotifyPropertyChanged
    {
        private ObservableCollection<NewsItemVM> _items;
        public ObservableCollection<NewsItemVM> Items
        {
            get
            {
                return _items;
            }
            set
            {
                if (_items != value)
                {
                    _items = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Items)));
                }
            }
        }

        private bool _isLoading;
        public bool IsLoading
        {
            get
            {
                return _isLoading;
            }
            set
            {
                if (_isLoading != value)
                {
                    _isLoading = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(IsLoading)));
                }
            }
        }

        private NewsItemVM _selectedItem;
        public NewsItemVM SelectedItem
        {
            get
            {
                return _selectedItem;
            }
            set
            {
                if (_selectedItem != value)
                {
                    _selectedItem = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(SelectedItem)));
                }
            }
        }

        public ICommand LogoutCommand { get; private set; }

        public event PropertyChangedEventHandler PropertyChanged;

        private INewsPage _page;

        public NewsPageVM(INewsPage page)
		{
            _page = page;

            Items = new ObservableCollection<NewsItemVM>();

            LogoutCommand = new Command(Logout);

            LoadNews();

            PropertyChanged += MainPageVM_PropertyChanged;
        }

        private async void Logout()
        {
            try
            {
                IsLoading = true;

                await Context.LoginService.Logout();
            }
            catch (Exception ex)
            {
                await Context.NotificationService.ShowNotification(ex.ToString());
            }
            finally
            {
                IsLoading = false;
                _page.ShowLoginPage();
            }
        }

        private void MainPageVM_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(SelectedItem))
            {
                SelectedItemChanged();
            }
        }

        public void SelectedItemChanged()
        {
            if (SelectedItem == null)
                return;

            var newsItemPageVM = new NewsItemPageVM(SelectedItem.NewsDTO);

            _page.ShowItemNewsPage(newsItemPageVM);

            SelectedItem = null;
        }


        private async void LoadNews()
        {
            try
            {
                this.IsLoading = true;
                var newsResult = await Context.NewsService.LoadListNews();

                Items.Clear();

                foreach (var item in newsResult)
                {
                    Items.Add(new NewsItemVM(item)
                    {
                        Title = item.Title,
                        Description = item.Description,
                        ImageUrl = item.UrlToImage
                    });
                }
            }
            catch (Exception ex)
            {
                if (ex is OperationCanceledException)
                    return;

                await Context.NotificationService.ShowNotification(ex.ToString());
            }
            finally
            {
                this.IsLoading = false;
            }
        }

    }
}