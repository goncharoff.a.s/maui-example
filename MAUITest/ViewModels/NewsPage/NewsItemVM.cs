﻿using System;
using System.ComponentModel;
using MAUITest.Services.DTOs;

namespace MAUITest.ViewModels
{
    public class NewsItemVM
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string ImageUrl { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        public ListItemNewsDTO NewsDTO { get; private set; }

        public NewsItemVM(ListItemNewsDTO newsDTO)
        {
            NewsDTO = newsDTO;
        }
    }
}