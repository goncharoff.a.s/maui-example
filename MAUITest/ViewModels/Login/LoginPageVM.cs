﻿using System;
using System.ComponentModel;
using System.Windows.Input;
using MAUITest.Services;
using MAUITest.ViewModels.Login;

namespace MAUITest.ViewModels
{
	public class LoginPageVM : INotifyPropertyChanged
	{
        public string LoginText { get; set; }

        //public string PasswordText { get; set; }

        private string _passwordText;
        public string PasswordText
        {
            get
            {
                return _passwordText;
            }
            set
            {
                if (_passwordText != value)
                {
                    _passwordText = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(PasswordText)));
                }
            }
        }

        public ICommand LoginCommand { get; private set; }

        private bool _isLoading;
        public bool IsLoading
        {
            get
            {
                return _isLoading;
            }
            set
            {
                if (_isLoading != value)
                {
                    _isLoading = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(IsLoading)));
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private ILoginPage _page;
        public LoginPageVM(ILoginPage page)
		{
            _page = page;
            LoginCommand = new Command(MakeAuth);
        }

        private async void MakeAuth()
        {
            try
            {
                IsLoading = true;
                //var loginResult = await Service<ILoginService>.GetInstance().Login(LoginText, PasswordText);
                var loginResult = await Context.LoginService.Login(LoginText, PasswordText);
                IsLoading = false;
                _page.OpenMainPage();
            }
            catch (Exception ex)
            {
                IsLoading = false;
                await Context.NotificationService.ShowNotification(ex.Message);
                PasswordText = "";
            }
        }
    }
}