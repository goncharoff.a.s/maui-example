﻿using MAUITest.Services;
using MAUITest.Services.Fakes;
using MAUITest.Services.Implementations;
using Microsoft.Extensions.Logging;

namespace MAUITest;

public static class MauiProgram
{
	public static MauiApp CreateMauiApp()
	{
		var useFake = true;

        Service<INotificationService>.RegisterService(new NotificationService());
        if (useFake)
		{
			Service<ILoginService>.RegisterService(new FakeLoginService());
			Service<INewsService>.RegisterService(new FakeNewsService());
        }
		else
        {
            //Service<ILoginService>.RegisterService(new LoginService());
        }

		var builder = MauiApp.CreateBuilder();
		builder
			.UseMauiApp<App>()
			.ConfigureFonts(fonts =>
			{
				fonts.AddFont("OpenSans-Regular.ttf", "OpenSansRegular");
				fonts.AddFont("OpenSans-Semibold.ttf", "OpenSansSemibold");
			});

#if DEBUG
        builder.Logging.AddDebug();
#endif

		return builder.Build();
	}
}

