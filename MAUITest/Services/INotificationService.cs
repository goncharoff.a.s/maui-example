﻿using System;
namespace MAUITest.Services
{
	public interface INotificationService
	{
        public Task ShowNotification(string message);
    }
}

