﻿using System;
namespace MAUITest.Services.Implementations
{
	public class NotificationService : INotificationService
	{
        public Task ShowNotification(string message)
        {
            var page = Application.Current.MainPage.Navigation.NavigationStack.LastOrDefault();
            return page?.DisplayAlert("Ошибка", message, "Ок");
        }

    }
}

