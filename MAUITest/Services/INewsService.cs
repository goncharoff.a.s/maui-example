﻿using System;
using MAUITest.Services.DTOs;

namespace MAUITest.Services
{
    public interface INewsService
    {
        Task<List<ListItemNewsDTO>> LoadListNews();
        Task<NewsDTO> LoadNews(long id);
    }
}