﻿using System;
using MAUITest.Services.DTOs;

namespace MAUITest.Services
{
	public interface ILoginService
	{
        public Task<bool> Login (string login, string password);
        public Task<UserDTO> GetCurrentUser();
        public bool IsLogin { get; }
        public Task<bool> Logout();
    }
}
