﻿using System;
namespace MAUITest.Services
{
    public static class Service<T>
    {
        private static T _t;

        public static void RegisterService(T service)
        {
            if (_t != null)
                throw new Exception("Сервис с таким типом уже зарегистрирован");
            _t = service;
        }

        public static T GetInstance()
        {
            return _t;
        }
    }
}