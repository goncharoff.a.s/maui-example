﻿using System;
using MAUITest.Services.DTOs;

namespace MAUITest.Services.Fakes
{
    public class FakeNewsService : INewsService
    {
        public FakeNewsService()
        {
        }

        public async Task<List<ListItemNewsDTO>> LoadListNews()
        {
            await Task.Delay(2000);

            return FakesData.NEWS.Select(x => new ListItemNewsDTO
            {
                Id = x.Id,
                Title = x.Title,
                Description = x.Description,
                UrlToImage = x.UrlToImage
            }).ToList();
        }

        public async Task<NewsDTO> LoadNews(long id)
        {
            await Task.Delay(2000);

            var news = FakesData.NEWS.FirstOrDefault(x => x.Id == id);

            if (news == null)
                throw new Exception("Такой новости не существует");

            return news;
        }
    }
}