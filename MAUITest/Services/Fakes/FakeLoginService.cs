﻿using System;
using MAUITest.Services.DTOs;

namespace MAUITest.Services.Fakes
{
    public class FakeLoginService : ILoginService
    {
        public const string CONNECTION_ID = "connection_id";

        private UserDTO _fakeUser = new UserDTO
        {
            Id = "1",
            Name = "Иван",
            LastName = "Сидоров"
        };

        private Guid? _accessToken { get; set; }


        public bool IsLogin
        {
            get
            {
                if (_accessToken != null)
                    return true;

                Guid guid;
                if (Guid.TryParse(Preferences.Get(CONNECTION_ID, null), out guid))
                {
                    _accessToken = guid;
                    return true;
                }
                return false;
            }
        }


        public async Task<bool> Login(string login, string password)
        {
            await Task.Delay(2000);

            if (login == "123" && password == "321")
            {
                _accessToken = Guid.NewGuid();

                Preferences.Set(CONNECTION_ID, _accessToken.ToString());
                return true;
            }

            throw new Exception("Неправильный логин или пароль!");
        }

        public async Task<bool> Logout()
        {
            await Task.Delay(2000);

            Preferences.Remove(CONNECTION_ID);

            _accessToken = null;
            return true;
        }

        public async Task<UserDTO> GetCurrentUser()
        {
            await Task.Delay(2000);

            if (IsLogin)
            {
                return _fakeUser;
            }
            throw new Exception("Пользователь не найден");
        }
    }
}