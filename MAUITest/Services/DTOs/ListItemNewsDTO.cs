﻿using System;
namespace MAUITest.Services.DTOs
{
	public class ListItemNewsDTO
	{
        public long Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string UrlToImage { get; set; }
    }
}

