﻿using System;
namespace MAUITest.Services.DTOs
{
	public class LoginDTO
	{
		public Guid AccessToken { get; set; }

		public UserDTO User { get; set; }
	}
}

