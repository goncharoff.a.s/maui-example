﻿using System;
namespace MAUITest.Services.DTOs
{
	public class UserDTO
	{
        public string Id { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }

    }
}

